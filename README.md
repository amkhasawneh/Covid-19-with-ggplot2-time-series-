# Covid-19-with-ggplot2

Here I try to make a "time series" plot of new Covid-19 cases against time for each continent. Here is the link to the dataset: https://github.com/owid/covid-19-data/tree/master/public/data

The graph on Jordanian data has black dots that represent new vaccinations against time.
